<?php

return [
    'batchsize' => env('PARSER_BATCH_SIZE', 100),
];
