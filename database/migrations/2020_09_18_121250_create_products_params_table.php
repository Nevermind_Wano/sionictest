<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_params', function (Blueprint $table) {
            $table->integer('code')->unsigned();
            $table->string('city_id');
            $table->integer('quantity')->unsigned()->default(0);
            $table->float('price')->default('0.0');
            $table->primary(['code', 'city_id']);

            $table->foreign('code')->on('products')->references('code');
            $table->foreign('city_id')->on('city')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_params');
    }
}
