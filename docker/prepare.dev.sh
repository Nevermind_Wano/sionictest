#!/usr/bin/env sh

. ./vars.sh

echoError() {
  echo "\033[1;31m\033[4m$1"
  tput sgr0
  exit
}

echoInfo() {
  echo '\033[1m\033[4m'$1
  tput sgr0
}

if ! command -v docker > /dev/null
then
    echoError  "Docker not found!"
fi

if ! command -v docker-compose > /dev/null
then
    echoError "Docker-compose not found!"
fi

if [ -f "../.env" ]; then
  echoInfo "Move laravel .env to .env.old"
  tput sgr0
  mv ../.env ../.env.old
fi

cp ./php/dev.env ../.env

echoInfo "Restart containers."
docker-compose down --remove-orphans --volumes && \
docker-compose up --build -d && \

echoInfo "Waiting for database..." && \
docker exec $POSTGRES_CONTAINER timeout 22 bash -c 'until printf "" >>/dev/tcp/localhost/5432; do sleep 1; done' && \

echoInfo "Install dependences."
docker exec $APPLICATION sh -c 'composer install --no-progress --no-interaction --no-scripts --optimize-autoloader --prefer-dist'

# echoInfo "Install node dependences."
# docker exec $NODE sh -c 'npm install'

echoInfo "Run migrations."
docker exec $APPLICATION sh -c 'php artisan migrate --seed'

tput sgr0
