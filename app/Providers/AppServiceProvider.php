<?php

namespace App\Providers;

use App\Repository\RepositoryFactory;
use App\Services\Logger;
use App\Services\ProductsService;
use App\Services\XmlParserService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(XmlParserService::class, function()
        {
            return new XmlParserService();
        });

        $this->app->singleton(Logger::class, function()
        {
            return new Logger();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
