<?php


namespace App\Services;


use Psr\Log\LoggerInterface;

class Logger implements LoggerInterface
{
    private $startTimes = [];

    public function emergency($message, array $context = [])
    {
        // TODO: Implement emergency() method.
    }

    public function alert($message, array $context = [])
    {
        // TODO: Implement alert() method.
    }

    public function critical($message, array $context = [])
    {
        // TODO: Implement critical() method.
    }

    public function error($message, array $context = [])
    {
        // TODO: Implement error() method.
    }

    public function warning($message, array $context = [])
    {
        // TODO: Implement warning() method.
    }

    public function notice($message, array $context = [])
    {
        // TODO: Implement notice() method.
    }

    public function info($message, array $context = [])
    {

    }

    public function debug($message, array $context = [])
    {

    }

    public function log($level, $message, array $context = [])
    {
        $this->$level($message, $context);
    }

    public function trace($label = 'start', $calculateTime = false, $message = '')
    {
        if (php_sapi_name() !== 'cli')
            return; // TODO просто пишем в лог-файл

        if (!$calculateTime)
        {
            $this->startTimes[$label] = microtime(true);
            echo "RUN $label $message" . PHP_EOL;
            return;
        }
        $time = microtime(true) - $this->startTimes[$label];
        echo "END $label $message / TIME: $time" . PHP_EOL;

    }
}
