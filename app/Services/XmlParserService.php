<?php

namespace App\Services;

use App\DTO\City;
use App\DTO\Product;
use App\DTO\ProductParams;
use App\Exceptions\FileTypeNotXml;

use App\Handlers\CityHandler;
use App\Handlers\HandlerInterface;
use App\Handlers\ProductHandler;
use App\Handlers\ProductParamsHandler;
use Illuminate\Support\Facades\File;
use SimpleXMLElement;
use Symfony\Component\Finder\SplFileInfo;
use Orchestra\Parser\Xml\Facade as XmlParser;

/**
 * Class XmlParserService
 *
 *
 * @package App\Services
 */
class XmlParserService
{
    const DEFAULT_OFFERS_FILE_NAME = 'offers';
    const DEFAULT_IMPORT_FILE_NAME = 'import';

    /** @var $offersFileName string */
    protected $offersFileName;

    /** @var $importFileName string */
    protected $importFileName;

    /** @var  $directoryPath string */
    protected $directoryPath;

    /** @var $files SplFileInfo[] */
    protected $files;

    /** @var $collection Product[] */
    protected $collection = [];

    /** @var $handlers HandlerInterface[] */
    protected $handlers = [];

    private $logger;

    /**
     * Запускает процесс прасинга
     *
     * @param string $directoryPath Путь к директории с xml-фалами
     * @param Logger $logger
     * @param string $importFileName Название файлов с каталогом товаров.
     * @param string $offersFileName Название файлов с пакетом предложений.
     *
     * @return XmlParserService
     */
    public function run(
        string $directoryPath,
        Logger $logger,
        string $importFileName = XmlParserService::DEFAULT_IMPORT_FILE_NAME,
        string $offersFileName = XmlParserService::DEFAULT_OFFERS_FILE_NAME
    ): XmlParserService {
        $this->logger = $logger;
        $this->importFileName = $importFileName;
        $this->offersFileName = $offersFileName;
        $this->directoryPath = $directoryPath;
        $this->files = File::allFiles($this->directoryPath);


        $this->logger->trace('parsing');
        $this->getContentsFromFiles();
        $this->logger->trace('parsing', true);

        return $this;
    }

    /**
     * Добавляет обработчики
     *
     * @param HandlerInterface ...$handlers
     * @return $this
     */
    public function setHandlers(HandlerInterface ...$handlers)
    {
        foreach ($handlers as $handler)
            $this->handlers[$handler->getType()] = $handler;


        return $this;
    }

    protected function getContentsFromFiles(): void
    {
        foreach ($this->files as $file)
        {
            try
            {
                if ($file->getExtension() !== 'xml')
                    throw new FileTypeNotXml($file);

                $filename = $file->getFilename();

                $this->logger->trace('parsing file', false, $filename);

                if (strpos($filename, $this->offersFileName) !== false)
                    $this->parseOffersFile($file, $this->handlers[ProductParamsHandler::TYPE]);
                elseif (strpos($filename, $this->importFileName) !== false)
                    $this->parseImportFile($file, $this->handlers[ProductHandler::TYPE]);

                $this->logger->trace('parsing file', true, $filename);

            } catch (FileTypeNotXml $e)
            {
                // TODO send to logger
                continue;
            }
        }
    }

    private function parseImportFile(SplFileInfo $file, HandlerInterface $handler): void
    {
        $xml = XmlParser::load($file->getRealPath());
        $this->handleCity($file->getFilename(), $xml);

        foreach ($xml->getContent()->Каталог->Товары->Товар as $product)
        {
            $code = (int)$product->Код;

            // Если товар уже есть в коллекции - идём дальше.
            if (!empty($this->collection[$code]) && !empty($this->collection[$code]->getName()))
                continue;

            $this->collection[$code] = new Product($code);
            $this->collection[$code]->setName($product->Наименование)
                ->setWeight($product->Вес);

            if (isset($product->Взаимозаменяемости))
                $this->collection[$code]->setUsages($this->getUsages($product->Взаимозаменяемости->Взаимозаменяемость));

            // Отправляем товар обработчику
            $handler->handle($this->collection[$code]);
        }
    }

    private function parseOffersFile(SplFileInfo $file, HandlerInterface $handler): void
    {
        $cityCode = $this->getCityCode($this->offersFileName, $file->getFilename());
        $xml = XmlParser::load($file->getRealPath());

        foreach ($xml->getContent()->ПакетПредложений->Предложения->Предложение as $offer)
        {
            $code = (int)$offer->Код;

            $productParams = new ProductParams();
            $productParams->setPrice((float)$offer->Цены->Цена[0]->ЦенаЗаЕдиницу)
                ->setQuantity((int)$offer->Количество)
                ->setProductCode((int)$code)
                ->setCityId($cityCode);

            $handler->handle($productParams);
        }
    }

    private function getCityCode(string $filename, string $realFilename): string
    {
        $replacedFileName = str_replace($filename, '', $realFilename);

        return (string)substr($replacedFileName, 0, strpos($replacedFileName, '_'));
    }

    private function getUsages(SimpleXMLElement $usages): string
    {
        $result = [];
        foreach ($usages as $key => $value)
        {
            $_result = [];
            foreach ($value as $item)
            {
                $_result[] = (string)$item;
            }

            $result[] = implode('-', $_result);
        }

        return implode('|', $result);
    }

    private function generateHash(array $arr)
    {
        // TODO реализовать
    }

    /**
     * @param string $filename
     * @param \Laravie\Parser\Document $xml
     */
    private function handleCity(string $filename, \Laravie\Parser\Document $xml): void
    {
        $cityCode = $this->getCityCode($this->importFileName, $filename);
        if (isset($xml->getContent()->Каталог->Наименование))
            $this->handlers[CityHandler::TYPE]->handle(new City($cityCode, (string)$xml->getContent()->Каталог->Наименование));
    }
}
