<?php

declare(strict_types=1);

namespace App\Handlers;


use App\DTO\DTOHasCityIdInterface;
use App\DTO\DTOInterface;
use App\Repository\EntityInterface;
use App\Repository\RepositoryFactory;

/**
 * Class ProductParamsStrictHandler
 * @package App\Handlers
 */
final class ProductParamsStrictHandler extends ProductParamsHandler
{
    const QUANTITY_PREFIX = 'quantity_';
    const PRICE_PREFIX    = 'price_';

    private $cityEntity;
    private $citiesSlugs = [];
    private $paramsData = [];
    private $productStubFields = [];

    /**
     * ProductParamsStrictHandler constructor.
     * @param EntityInterface $productEntity
     * @param EntityInterface $cityEntity
     */
    public function __construct(EntityInterface $productEntity, EntityInterface $cityEntity)
    {
        parent::__construct($productEntity);
        $this->cityEntity = $cityEntity;
        $this->setProductStubFields();
    }

    /**
     * Создаем массив параметров товара и добавляем его в массив $this->paramsData,
     * с ключом равным коду товара
     *
     * @param DTOInterface $item
     * @return $this|HandlerInterface
     * @throws \Exception
     */
    public function handle(DTOInterface $item): HandlerInterface
    {
        if (!$item instanceof DTOHasCityIdInterface)
            throw new \Exception('Объект не является типом DTOHasCityIdInterface', 400);

        if (!isset($this->paramsData[$item->code]))
            $this->paramsData[$item->code] = [];

        $citySlug = $this->getCitySlug($item);
        $this->paramsData[$item->code] = array_merge($this->paramsData[$item->code], [
            'code'                            => $item->code,
            self::PRICE_PREFIX .  $citySlug   => $item->price,
            self::QUANTITY_PREFIX . $citySlug => $item->quantity
        ]);

        return $this;
    }

    /**
     * @return mixed|void
     * @throws \App\Exceptions\BatchIsEmpty
     */
    public function execute()
    {
        $this->clearBatchCounter();
        foreach($this->paramsData as $item)
        {
            $this->batchCounter();
            $this->data[$this->numberSubBatch][] = array_merge($this->fixEmptyColumn($item), $this->productStubFields);
        }

        parent::execute();
    }

    /** @inheritDoc */
    protected function getFields(): array
    {
        return array_keys(current($this->paramsData));
    }

    /** @inheritDoc */
    protected function getTargetField(): array
    {
        return ProductHandler::TARGET_FIELDS;
    }

    /**
     * Получаем псевдоним названия города из массива,
     * если его нет получаем все псевдонимы из БД
     *
     * @param DTOHasCityIdInterface $item
     * @return string
     */
    private function getCitySlug(DTOHasCityIdInterface $item): string
    {
        if (!isset($this->citiesSlugs[$item->getCityId()]))
            $this->setCitiesSlugs(RepositoryFactory::create($this->cityEntity)->getAll());

        return $this->citiesSlugs[$item->getCityId()];
    }


    /**
     * @param mixed $paramsData
     * @return ProductParamsStrictHandler
     */
    public function setParamsData($paramsData)
    {
        $this->paramsData = $paramsData;

        return $this;
    }

    /**
     * @param EntityInterface $cityEntity
     * @return ProductParamsStrictHandler
     */
    public function setCityEntity(EntityInterface $cityEntity): ProductParamsStrictHandler
    {
        $this->cityEntity = $cityEntity;

        return $this;
    }

    /**
     * @param array $cities
     */
    private function setCitiesSlugs(array $cities): void
    {
        foreach ($cities as $city)
            $this->citiesSlugs[$city['id']] = $city['slug'];
    }


    /**
     * Заполняем ннедостающие поля в таблице products заглушками
     * Нужно для корректной работы метода INSERT ... ON CONFLICT .. DO UPDATE
     */
    private function setProductStubFields(): void
    {
        foreach (ProductHandler::FIELDS as $field)
            if (!in_array($field, ProductHandler::TARGET_FIELDS)) $this->productStubFields[$field] = 0;
    }

    /**
     * Проверяет наличие полей в одном конкретном наборе параметров товара,
     * и при отсутсвии такого поля устанавливается значение по умолчанию. Нужно для корректной работы
     * метода INSERT ... ON CONFLICT .. DO UPDATE
     *
     * @param array $array
     * @return array
     */
    private function fixEmptyColumn(array $array)
    {
        foreach($this->citiesSlugs as $slug)
        {
            if (!array_key_exists(self::PRICE_PREFIX . $slug, $array))
                $array[self::PRICE_PREFIX . $slug] = 0.0;
            if (!array_key_exists(self::QUANTITY_PREFIX . $slug, $array))
                $array[self::QUANTITY_PREFIX . $slug] = 0;
        }

        return $array;
    }


}
