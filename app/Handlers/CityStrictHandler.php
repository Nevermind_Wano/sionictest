<?php


namespace App\Handlers;


use App\DTO\DTOInterface;
use App\Repository\EntityInterface;
use App\Repository\RepositoryFactory;
use App\Repository\RepositoryInterface;


/**
 * Class CityStrictHandler
 * @package App\Handlers
 */
final class CityStrictHandler extends CityHandler
{
    /** @var $productRepository RepositoryInterface */
    private $productRepository;

    /**
     * CityStrictHandler constructor.
     * @param EntityInterface $cityEntity
     * @param EntityInterface $productEntity
     */
    public function __construct(EntityInterface $cityEntity, EntityInterface $productEntity)
    {
        $this->productRepository = RepositoryFactory::create($productEntity);
        parent::__construct($cityEntity);
    }

    /**
     * @param \App\DTO\City $city
     * @return $this|HandlerInterface
     */
    public function handle(DTOInterface $city): HandlerInterface
    {
        $city->setSlug(substr($this->rus2translit($city->name), 0, 4));
        $this->productRepository->addColumn(ProductParamsStrictHandler::QUANTITY_PREFIX . $city->getSlug(), 'integer');
        $this->productRepository->addColumn(ProductParamsStrictHandler::PRICE_PREFIX . $city->getSlug(), 'float');

        return parent::handle($city);
    }

    /**
     * @param string $string
     * @return string
     */
    private function rus2translit(string $string)
    {
        $converter = [
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        ];

        return strtr($string, $converter);
    }
}
