<?php


namespace App\Handlers;


use App\DTO\DTOInterface;
use App\Repository\EntityInterface;
use App\Repository\RepositoryFactory;

class ProductStrictHandler extends ProductHandler
{
    /** @var array */
    private $cities;

    public function __construct(EntityInterface $entity, EntityInterface $cityEntity)
    {
        parent::__construct($entity);
        $this->cities = RepositoryFactory::create($cityEntity)->getAll();
    }

    public function handle(DTOInterface $item): HandlerInterface
    {
        return $this;
    }
}
