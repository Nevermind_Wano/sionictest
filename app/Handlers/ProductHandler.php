<?php


namespace App\Handlers;


class ProductHandler extends BatchInsertHandler
{
    const TYPE = 'ProductHandler';
    const FIELDS = [
        'code', 'name', 'weight', 'usage'
    ];
    const TARGET_FIELDS = ['code'];

}
