<?php


namespace App\Handlers;


use App\DTO\DTOInterface;

/**
 * Interface HandlerInterface
 * @package App\Handlers
 */
interface HandlerInterface
{
    /**
     * Тип обработчика. Задаётся константой в конкретном классе обрботчика
     *
     * @return string
     */
    public function getType(): string;

    /**
     * @param DTOInterface $item
     * @return HandlerInterface
     */
    public function handle(DTOInterface $item): HandlerInterface;

    /**
     * @return mixed
     */
    public function execute();
}
