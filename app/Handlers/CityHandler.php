<?php


namespace App\Handlers;


class CityHandler extends InsertHandler
{
    const TYPE = 'CityHandler';
    const FIELDS = [
        'id', 'name', 'slug'
    ];
    const TARGET_FIELDS = ['id'];
}
