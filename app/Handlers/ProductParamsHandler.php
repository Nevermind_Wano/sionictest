<?php


namespace App\Handlers;


class ProductParamsHandler extends BatchInsertHandler
{
    const TYPE = 'ProductParamsHandler';
    const FIELDS = [
        'code', 'city_id', 'quantity', 'price'
    ];
    const TARGET_FIELDS = ['code', 'city_id'];
}
