<?php


namespace App\Handlers;

use App\DTO\DTOInterface;
use App\Exceptions\BatchIsEmpty;
use App\Repository\EntityInterface;

/**
 * Обработчик пакетной записи данных в БД
 *
 * Class BatchInsertHandler
 * @package App\Handlers
 */
abstract class BatchInsertHandler extends InsertHandler implements HandlerInterface
{
    protected $data = [];
    protected $subBatchCount = 0;
    protected $numberSubBatch = 0;
    protected $batchSize = 100;

    /**
     * BatchInsertHandler constructor.
     * @param EntityInterface $entity
     */
    public function __construct(EntityInterface $entity)
    {
        parent::__construct($entity);
        $this->batchSize = config('parser.batchsize');
    }

    /** @inheritDoc */
    public function handle(DTOInterface $item): HandlerInterface
    {
        $this->batchCounter();
        $this->data[$this->numberSubBatch][] = $this->createKeyValueToInsert($item);
        return $this;
    }

    /** @inheritDoc */
    public function execute()
    {
        if (empty($this->data))
            throw new BatchIsEmpty();

        foreach ($this->data as $key => $data)
        {
            $this->repository->setData($data);
            parent::execute();
        }
    }


    /**
     * Счётчик количества элементов в пачке
     */
    protected function batchCounter(): void
    {
        $this->subBatchCount++;

        if ($this->subBatchCount >= $this->batchSize)
        {
            $this->subBatchCount = 0;
            $this->numberSubBatch++;
        }
    }

    protected function clearBatchCounter()
    {
        $this->subBatchCount = 0;
        $this->subBatchCount = 0;
    }


}
