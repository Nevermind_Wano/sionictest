<?php


namespace App\Handlers;

use App\DTO\DTOInterface;
use App\Repository\EntityInterface;
use App\Repository\RepositoryFactory;
use App\Repository\RepositoryInterface;

/**
 * Class InsertHandler
 * @package App\Handlers
 */
abstract class InsertHandler implements HandlerInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;
    protected $type;

    /**
     * InsertHandler constructor.
     * @param EntityInterface $entity
     */
    public function __construct(EntityInterface $entity)
    {
        $this->repository = RepositoryFactory::create($entity);
    }

    /** @inheritDoc */
    public function getType(): string
    {
        return static::TYPE;
    }

    /** @inheritDoc */
    public function handle(DTOInterface $item): HandlerInterface
    {
        $this->repository->setData($this->createKeyValueToInsert($item));
        $this->execute();
        return $this;
    }

    /** @inheritDoc */
    public function execute()
    {
        $this->repository->save($this->getTargetField(), $this->getFields());
    }

    /**
     * Получить поля в которые нужно сделать insert/update
     * @return array
     */
    protected function getFields(): array
    {
        return static::FIELDS;
    }

    /**
     * Получить уникальные поля по которым происходит поиск для обновления значений
     * @return array
     */
    protected function getTargetField(): array
    {
        return static::TARGET_FIELDS;
    }

    /**
     * Создаёт массив значений пригодный для записи в БД
     *
     * @param DTOInterface $object
     * @param array $returnArr
     * @return array
     */
    protected function createKeyValueToInsert(DTOInterface $object, array $returnArr = []): array
    {
        foreach (static::getFields() as $field)
        {
            $value = $object->$field;
            if (is_scalar($value))
            {
                $value = str_replace("'", "\"", $value);
                $returnArr[$field] = $value;
            }
        }

        return $returnArr;
    }
}
