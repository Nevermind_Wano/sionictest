<?php


namespace App\Exceptions;

use Monolog\Logger;


class BatchIsEmpty extends \Exception
{
    public function __construct()
    {
        parent::__construct('Нет данных для пакетного insert в базу.
        Сначала добавьте данные с помощью метода ProductsRepository::addToBatch() или
        ProductsRepository::setBatch()', Logger::ERROR);
    }
}
