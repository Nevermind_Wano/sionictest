<?php


namespace App\Exceptions;

use Monolog\Logger;


class BatchNotEmpty extends \Exception
{
    public function __construct($message = '', $code = Logger::WARNING)
    {
        $message = 'В ProductsRepository::$batch есть данные, которые исчезнут после данного действия. ' . $message;
        parent::__construct($message, $code);
    }
}
