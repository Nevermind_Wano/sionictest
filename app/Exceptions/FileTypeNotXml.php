<?php


namespace App\Exceptions;


use Monolog\Logger;
use SplFileInfo;

class FileTypeNotXml extends \Exception
{
    public function __construct(SplFileInfo $file, $code = Logger::WARNING)
    {
        $message = 'Файл . ' . $file->getFilename() . ' не является xml-файлом. Путь = ' . $file->getRealPath();
        parent::__construct($message, $code);
    }
}
