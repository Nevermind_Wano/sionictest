<?php


namespace App\Exceptions;


use Monolog\Logger;

class NotEqualCityCodesInPricesAndQuantity extends \Exception
{
    public function __construct(string $message = '', $code = Logger::ERROR)
    {
        $message = (empty($message)) ? 'Не совпадают коды городов в масиве prices и quantity' : $message;
        parent::__construct($message, $code);
    }
}
