<?php


namespace App\DTO;

class ProductParams implements DTOHasCityIdInterface
{
    private $code;
    private $city_id;
    private $price = 0;
    private $quantity = 0;

    /** @inheritDoc */
    public function __get($field)
    {
        return $this->$field;
    }

    /**
     * @return mixed
     */
    public function getCityId(): int
    {
        return $this->city_id;
    }

    /**
     * @param mixed $city_id
     * @return ProductParams
     */
    public function setCityId(int $city_id)
    {
        $this->city_id = $city_id;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getProductCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $product_code
     * @return ProductParams
     */
    public function setProductCode($product_code)
    {
        $this->code = $product_code;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return ProductParams
     */
    public function setPrice(float $price): ProductParams
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param mixed $quantity
     * @return ProductParams
     */
    public function setQuantity(int $quantity): ProductParams
    {
        $this->quantity = $quantity;

        return $this;
    }
}
