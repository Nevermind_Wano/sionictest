<?php


namespace App\DTO;


/** @inheritDoc */
class City implements DTOHasCityIdInterface
{

    private $id;
    private $name;
    private $slug = '';


    public function __construct(string $id, string $city)
    {
        $this->setId($id);
        $this->setCity($city);
    }

    /** @inheritDoc */
    public function __get($field)
    {
        return $this->$field;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return City
     */
    public function setSlug(string $slug)
    {
        $this->slug = strtolower($slug);

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return City
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity(): array
    {
        return $this->name;
    }

    /**
     * @param mixed $city
     * @return City
     */
    public function setCity(string $city): City
    {
        $this->name = $city;
        return $this;
    }

    public function getCityId(): int
    {
        return $this->id;
    }
}
