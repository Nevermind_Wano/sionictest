<?php


namespace App\DTO;


/**
 * DTO класс содержит поле cityId
 *
 * Interface DTOHasCityIdInterface
 * @package App\DTO
 */
interface DTOHasCityIdInterface extends DTOInterface
{
    /**
     * @return int
     */
    public function getCityId(): int;
}
