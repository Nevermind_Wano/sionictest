<?php


namespace App\DTO;

class Product implements DTOInterface
{
    private $code;
    private $name;
    private $weight = 0;
    private $usage  = '';

    public function __construct(int $code = 0)
    {
        if ($code !== 0)
            $this->setCode($code);
    }

    /** @inheritDoc */
    public function __get($field)
    {
        return $this->$field;
    }

    /**
     * @return string
     */
    public function getUsages(): string
    {
        return $this->usage;
    }

    /**
     * @param string $usages
     * @return Product
     */
    public function setUsages(string $usages): Product
    {
        $this->usage = $usages;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = (string)$name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     * @return Product
     */
    public function setWeight($weight)
    {
        $this->weight = (float)$weight;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return (int)$this->code;
    }

    /**
     * @param mixed $code
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
}
