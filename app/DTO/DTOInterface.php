<?php


namespace App\DTO;


interface DTOInterface
{
    /**
     * @param $field
     * @return mixed
     */
    public function __get($field);
}
