<?php


namespace App\Models;


/**
 * Trait EntityImplementation
 * @package App\Models
 */
trait EntityImplementation
{
    public function getAll(): array
    {
        return $this->all()->toArray();
    }

    public function getTableName(): string
    {
        return $this->getTable();
    }

    /**
     * Использует возможность PostrgreSQL INSERT ... ON CONFLICT .. DO UPDATE
     * При подключении других БД модель (или репозиторий) должна иметь другую
     * реализацию этого метода.
     *
     * @param array $data
     * @param array $targetField
     * @param array|null $columnsToUpdate
     */
    public function insertOrUpdate(array $data, array $targetField, array $columnsToUpdate = null): void
    {
        $targetField = implode(',', $targetField);
        $this->insertOnConflict($data, $columnsToUpdate, 'do update set', $targetField);
    }
}
