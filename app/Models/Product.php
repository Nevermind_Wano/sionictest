<?php

namespace App\Models;

use App\Repository\EntityInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements EntityInterface
{
    use HasFactory;
    use EntityImplementation;

    public function productParams()
    {
        return $this->hasOne('App\Models\ProductParams', 'product_code', 'code');
    }
}
