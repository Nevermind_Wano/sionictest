<?php

namespace App\Models;

use App\Repository\EntityInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model implements EntityInterface
{
    use HasFactory;
    use EntityImplementation;

    protected $table = 'city';
    protected $guarded = [];


}
