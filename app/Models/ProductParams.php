<?php

namespace App\Models;

use App\Repository\EntityInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductParams extends Model implements EntityInterface
{
    use HasFactory;
    use EntityImplementation;

    protected $table = 'products_params';

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
