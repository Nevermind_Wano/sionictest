<?php

namespace App\Console\Commands;

use App\Handlers\CityStrictHandler;
use App\Handlers\ProductHandler;
use App\Handlers\ProductParamsStrictHandler;
use App\Models\City;
use App\Models\Product;
use App\Models\ProductParams;
use App\Repository\RepositoryFactory;
use App\Services\Logger;
use App\Services\XmlParserService;

class ParseStrict extends Parse
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:strict';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse XML files. Strictly according to the technical task.';

    /**
     * Create a new command instance.
     *
     * @param XmlParserService $parser
     * @param RepositoryFactory $repositoryFactory
     */
    public function __construct(XmlParserService $parser, RepositoryFactory $repositoryFactory, Logger $logger)
    {
        parent::__construct($parser, $repositoryFactory, $logger);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->logger->trace('process');

        $cityHandler = new CityStrictHandler(new City(), new Product());
        $productHandler = new ProductHandler(new Product());
        $productParamsHandler = new ProductParamsStrictHandler(new Product(), new City());

        $this->parser->setHandlers($cityHandler, $productHandler, $productParamsHandler)
             ->run(storage_path('XMLData/'), $this->logger);


        $this->logger->trace('ProductHandler_execute');
            $productHandler->execute();
        $this->logger->trace('ProductHandler_execute', true);

        $this->logger->trace('ProductParamsHandler_execute');
            $productParamsHandler->execute();
        $this->logger->trace('ProductParamsHandler_execute', true);

        $this->logger->trace('process', true);

        return 0;
    }
}
