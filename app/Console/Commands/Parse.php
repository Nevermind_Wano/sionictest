<?php

namespace App\Console\Commands;

use App\Handlers\BatchInsertHandler;
use App\Handlers\CityHandler;
use App\Handlers\InsertHandler;
use App\Handlers\ProductHandler;
use App\Handlers\ProductParamsHandler;
use App\Models\City;
use App\Models\Product;
use App\Models\ProductParams;
use App\Repository\RepositoryFactory;
use App\Services\Logger;
use App\Services\XmlParserService;
use Illuminate\Console\Command;

class Parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse XML files';

    protected $parser;

    /* @var $repositoryFactory RepositoryFactory */
    private $repositoryFactory;

    protected $logger;


    /**
     * Create a new command instance.
     *
     * @param XmlParserService $parser
     * @param RepositoryFactory $repositoryFactory
     * @param Logger $logger
     */
    public function __construct(XmlParserService $parser, RepositoryFactory $repositoryFactory, Logger $logger)
    {
        parent::__construct();

        $this->parser = $parser;
        $this->repositoryFactory = $repositoryFactory;
        $this->logger = $logger;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->logger->trace('process');

        $cityHandler = new CityHandler(new City());
        $productHandler = new ProductHandler(new Product());
        $productParamsHandler = new ProductParamsHandler(new ProductParams());

        $this->parser->setHandlers($cityHandler, $productHandler, $productParamsHandler)
                     ->run(storage_path('XMLData/'), $this->logger);

        $this->logger->trace('ProductHandler_execute');
                $productHandler->execute();
        $this->logger->trace('ProductHandler_execute', true);

        $this->logger->trace('ProductParamsHandler_execute');
                $productParamsHandler->execute();
        $this->logger->trace('ProductParamsHandler_execute', true);


        $this->logger->trace('process', true);

        return 0;
    }
}
