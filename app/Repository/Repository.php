<?php


namespace App\Repository;

use Illuminate\Support\Facades\Schema;

class Repository implements RepositoryInterface
{
    protected $data = [];

    /* @var $entity EntityInterface */
    protected $entity;

    public function __construct(EntityInterface $entity)
    {
        $this->entity = $entity;
    }

    public function setData(array $data): RepositoryInterface
    {
        $this->data = $data;
        return $this;
    }

    // FIXME Здесь жесткая связанность с \Illuminate\Database\Schema\Builder.
    // Пока не придумал как сделать этот метод независимым. Какой-то адаптер или декоратор надо делать.
    public function addColumn(string $name, string $type): RepositoryInterface
    {
        if (Schema::hasColumn($this->getTableName(), $name))
            return $this;

        Schema::table($this->getTableName(), function($table) use ($name, $type)
        {
            $table->addColumn($type, $name)->nullable();
        });

        return $this;
    }

    public function getAll()
    {
        return $this->entity->getAll();
    }

    public function getTableName(): string
    {
        return $this->entity->getTableName();
    }

    public function setEntity(EntityInterface $entity): RepositoryInterface
    {
        $this->entity = $entity;
        return $this;
    }

    public function save(array $targetField = ['id'], array $columnsToUpdate = null)
    {
        try
        {
            $this->entity->insertOrUpdate($this->data, $targetField, $columnsToUpdate);
            $this->data = [];
        }
        catch(\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

}
