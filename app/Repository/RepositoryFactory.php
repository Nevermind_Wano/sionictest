<?php


namespace App\Repository;

class RepositoryFactory
{
    public static function create(EntityInterface $entity, string $repositoryName = null)
    {
        if (!$repositoryName)
            $repositoryName = Repository::class;
        return new $repositoryName(new $entity);
    }
}
