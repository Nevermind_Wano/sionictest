<?php


namespace App\Repository;

interface RepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAll();

    /**
     * @param array $data
     * @return RepositoryInterface
     */
    public function setData(array $data): RepositoryInterface;

    /**
     * @param EntityInterface $entity
     * @return RepositoryInterface
     */
    public function setEntity(EntityInterface $entity): RepositoryInterface;

    /**
     * @param string $name
     * @param string $type
     * @return RepositoryInterface
     */
    public function addColumn(string $name, string $type): RepositoryInterface;

    /**
     * @return string
     */
    public function getTableName(): string;

    /**
     * @param array|string[] $targetField
     * @return mixed
     */
    public function save(array $targetField = ['id']);
}
