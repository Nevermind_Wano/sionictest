<?php


namespace App\Repository;


/**
 * Сущность или модели, которые должны реализовывать данный интерфейс,
 * чтобы работать с функционалом репозитория
 *
 * Interface EntityInterface
 * @package App\Repository
 */
interface EntityInterface
{
    /**
     * @return array
     */
    public function getAll(): array;

    /**
     * @return string
     */
    public function getTableName(): string;

    /**
     * Функционал вставки или обновления полей в DB
     *
     * @param array $data массив данных в виде [$nameColumn => $value]
     * @param array $targetField Столбцы с уникальным значение или значениями
     */
    public function insertOrUpdate(array $data, array $targetField, array $columnsToUpdate = null): void;
}
