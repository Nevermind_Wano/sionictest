php := $(shell command -v php)

prepare:
	./prepare.dev.sh
parse:
	${php} artisan parse
parse_strict:
	${php} artisan parse:strict
resetdb:
	${php} artisan migrate:fresh
