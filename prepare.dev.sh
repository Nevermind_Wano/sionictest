#!/usr/bin/env sh

echoError() {
  echo "\033[1;31m\033[4m$1"
  tput sgr0
  exit
}

echoInfo() {
  echo '\033[1m\033[4m'$1
  tput sgr0
}

if ! command -v php > /dev/null
then
    echoError  "PHP not found!"
fi

PHPVERSION=$(php --version | head -n 1 | cut -d " " -f 2 | cut -c 1,3)
minimumRequiredVersion=73

if [ "$PHPVERSION" -lt "$minimumRequiredVersion" ];
then
    echoError  "PHP version must be $minimumRequiredVersion. Your PHP version is $PHPVERSION."
fi

if ! command -v psql > /dev/null
then
    echoError  "PostgreSQL not found!"
fi

if ! command -v composer > /dev/null
then
    echoError  "Composer not found!"
fi

if [ -f "./.env" ]; then
  echoInfo "Move laravel .env to .env.old"
  tput sgr0
  cp ./.env ./.env.old
fi

cp ./env.dev ./.env

echoInfo "Install dependences."
composer install --no-progress --no-interaction --no-scripts --optimize-autoloader --prefer-dist

echoInfo "Run migrations."
php artisan migrate

tput sgr0
